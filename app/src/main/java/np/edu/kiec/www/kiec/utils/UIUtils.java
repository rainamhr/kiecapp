package np.edu.kiec.www.kiec.utils;

import android.app.Activity;
import android.support.design.widget.NavigationView;

import np.edu.kiec.www.kiec.R;

/**
 * Created by sabin on 2/27/18.
 */

public class UIUtils {
    public static void setNavigationCheckedItem(Activity activity, int itemId) {
        ((NavigationView) activity.findViewById(R.id.nav_view)).setCheckedItem(itemId);
    }
}
