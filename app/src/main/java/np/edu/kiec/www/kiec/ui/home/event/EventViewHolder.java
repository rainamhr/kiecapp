package np.edu.kiec.www.kiec.ui.home.event;

import android.content.Context;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import np.edu.kiec.www.kiec.ui.base.AbstractViewHolder;
import np.edu.kiec.www.kiec.databinding.ItemEventBinding;
import np.edu.kiec.www.kiec.databinding.ViewHolderEventBinding;

/**
 * Created by sabin on 2/24/18.
 */

public class EventViewHolder extends AbstractViewHolder {
    ViewHolderEventBinding viewHolderEventsBinding;

    public EventViewHolder(ViewHolderEventBinding viewHolderEventsBinding) {
        super(viewHolderEventsBinding.getRoot());
        this.viewHolderEventsBinding = viewHolderEventsBinding;
    }


    @Override
    public void bind(int position) {
        EventAdapter eventAdapter = new EventAdapter();

        Context context = viewHolderEventsBinding.getRoot().getContext();

        viewHolderEventsBinding.eventRecyclerView.setLayoutManager(
                new LinearLayoutManager(context)
        );

        viewHolderEventsBinding.eventRecyclerView.addItemDecoration(
                new DividerItemDecoration(context, LinearLayoutManager.VERTICAL)
        );

        viewHolderEventsBinding.eventRecyclerView.setAdapter(eventAdapter);
    }

    class EventAdapter extends RecyclerView.Adapter<AbstractViewHolder> {
        List<Event> eventList;

        EventAdapter() {
            eventList = new ArrayList<>();
            eventList.add(
                    new Event("24 \nFeb",
                            "JOIN INFORMATION SESSION BY UNIVERSITY OF TASMANIA",
                            "Kathmandu Infosys - Pokhara",
                            "Event Orgainzed by: University of Tasmania")
            );


            eventList.add(
                    new Event("27 \nFeb",
                            "JOIN INFORMATION SESSION BY UNIVERSITY OF TASMANIA",
                            "Kathmandu Infosys - Pokhara",
                            "Event Orgainzed by: University of Tasmania")
            );


            eventList.add(
                    new Event("28 \nFeb",
                            "JOIN INFORMATION SESSION BY UNIVERSITY OF TASMANIA",
                            "Kathmandu Infosys - Pokhara",
                            "Event Orgainzed by: University of Tasmania")
            );
        }

        @Override
        public AbstractViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            ItemEventBinding itemEventBinding =
                    ItemEventBinding.inflate(
                            LayoutInflater.from(parent.getContext()),
                            parent,
                            false
                    );
            return new EventItemViewHolder(itemEventBinding);
        }

        @Override
        public void onBindViewHolder(AbstractViewHolder holder, int position) {
            holder.bind(position);
        }

        @Override
        public int getItemCount() {
            return eventList.size();
        }

        class EventItemViewHolder extends AbstractViewHolder {
            ItemEventBinding itemEventBinding;

            public EventItemViewHolder(ItemEventBinding itemEventBinding) {
                super(itemEventBinding.getRoot());
                this.itemEventBinding = itemEventBinding;
            }

            @Override
            public void bind(int position) {
                itemEventBinding.setEvent(eventList.get(position));
                itemEventBinding.executePendingBindings();
            }
        }
    }

}
