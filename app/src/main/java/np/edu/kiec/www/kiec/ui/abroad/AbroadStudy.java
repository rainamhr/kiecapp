package np.edu.kiec.www.kiec.ui.abroad;

/**
 * Created by sabin on 2/25/18.
 */

public class AbroadStudy {
    private String url;
    private String title;
    private String subtitle;

    public AbroadStudy(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public AbroadStudy setSubtitle(String subtitle) {
        this.subtitle = subtitle;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public AbroadStudy setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getTitle() {
        return title;
    }
}
