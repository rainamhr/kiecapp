package np.edu.kiec.www.kiec.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import np.edu.kiec.www.kiec.R;
import np.edu.kiec.www.kiec.ui.abroad.AbroadStudyFragment;
import np.edu.kiec.www.kiec.ui.careercounseling.CareerCounselingFragment;
import np.edu.kiec.www.kiec.ui.testpreparation.TestPreparationFragment;
import np.edu.kiec.www.kiec.utils.Constants;

public class SubMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_menu);

        int id = getIntent().getIntExtra("id", -1);

        if (id != -1) {
            showRespectiveFragment(id);
        }
    }

    private void showRespectiveFragment(int id) {
        switch (id) {
            case Constants.CAREER_COUNSELING:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.sub_menu_container, new CareerCounselingFragment())
                        .commit();
                break;

            case Constants.TEST_PREPARATION:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.sub_menu_container, new TestPreparationFragment())
                        .commit();
                break;
            case Constants.ABROAD_STUDIES:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.sub_menu_container, new AbroadStudyFragment())
                        .commit();
                break;
//            case Constants.BRANCHES:
//                getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.sub_menu_container, new BranchesFragment())
//                        .commit();
//                break;
//            case Constants.TOFEL:
//                getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.sub_menu_container, new T())
//                        .commit();
//                break;
            case Constants.IELTS:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.sub_menu_container, new CareerCounselingFragment())
                        .commit();
                break;
            case Constants.SAT:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.sub_menu_container, new CareerCounselingFragment())
                        .commit();
                break;
            case Constants.GRE:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.sub_menu_container, new CareerCounselingFragment())
                        .commit();
                break;
            case Constants.GMAT:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.sub_menu_container, new CareerCounselingFragment())
                        .commit();
                break;
            case Constants.PTE:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.sub_menu_container, new CareerCounselingFragment())
                        .commit();
                break;


        }
    }
}
