package np.edu.kiec.www.kiec.ui.home.menu;

/**
 * Created by sabin on 2/25/18.
 */

public interface MenuInterface {
    public void onMenuClicked(int id);
}
