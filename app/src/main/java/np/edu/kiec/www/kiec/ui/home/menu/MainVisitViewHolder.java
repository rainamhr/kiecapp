package np.edu.kiec.www.kiec.ui.home.menu;

import android.content.Context;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import np.edu.kiec.www.kiec.ui.base.AbstractViewHolder;
import np.edu.kiec.www.kiec.R;
import np.edu.kiec.www.kiec.databinding.ViewHolderMainBinding;
import np.edu.kiec.www.kiec.utils.Constants;

/**
 * Created by sabin on 2/24/18.
 */

public class MainVisitViewHolder extends AbstractViewHolder {
    ViewHolderMainBinding viewHolderMainBinding;

    public MainVisitViewHolder(ViewHolderMainBinding viewHolderMainBinding) {
        super(viewHolderMainBinding.getRoot());
        this.viewHolderMainBinding = viewHolderMainBinding;
    }

    @Override
    public void bind(int position) {
        List<MainVisit> mainVisitList;

        mainVisitList = new ArrayList<>();
        mainVisitList.add(
                new MainVisit(R.drawable.ic_carrer_counselling, "Career Counseling", Constants.CAREER_COUNSELING)
        );
        mainVisitList.add(
                new MainVisit(R.drawable.ic_test_prep, "Test Prep", Constants.TEST_PREPARATION)
        );
        mainVisitList.add(
                new MainVisit(R.drawable.ic_abroad_studies, "Abroad Studies", Constants.ABROAD_STUDIES)
        );
        mainVisitList.add(
                new MainVisit(R.drawable.ic_branch, "Branches", Constants.BRANCHES)
        );

        MenuAdapter mainVisitAdapter = new MenuAdapter(mainVisitList);


        Context context = viewHolderMainBinding.getRoot().getContext();

        viewHolderMainBinding.homeMainRecyclerView.setLayoutManager(
                new GridLayoutManager(context, 2)
        );

        viewHolderMainBinding.homeMainRecyclerView
                .addItemDecoration(new DividerItemDecoration(context,
                        LinearLayoutManager.HORIZONTAL));

        viewHolderMainBinding.homeMainRecyclerView
                .addItemDecoration(new DividerItemDecoration(context,
                        LinearLayoutManager.VERTICAL));

        viewHolderMainBinding.homeMainRecyclerView.setAdapter(mainVisitAdapter);
    }
}