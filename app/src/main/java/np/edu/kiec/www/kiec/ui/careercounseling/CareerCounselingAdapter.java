package np.edu.kiec.www.kiec.ui.careercounseling;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.util.ArrayList;

import np.edu.kiec.www.kiec.R;

/**
 * Created by chirag on 12/8/17.
 */

public class CareerCounselingAdapter extends RecyclerView.Adapter {
    private static final String TAG = "SubMenuListAdaptor";

    private ArrayList<CareerCounseling> items = new ArrayList<>();

    public CareerCounselingAdapter(ArrayList<CareerCounseling> items) {
        this.items = items;
    }

    public void setItems(ArrayList<CareerCounseling> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new SubMenuListHolder(inflater.inflate(R.layout.sub_menu_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        CareerCounseling item = items.get(position);
        if (holder instanceof SubMenuListHolder) {
            SubMenuListHolder h = (SubMenuListHolder) holder;
            if (item.getTitle().isEmpty()) {
                h.title.setVisibility(View.GONE);
            } else {
                h.title.setVisibility(View.VISIBLE);
                h.title.setText(Html.fromHtml(item.getTitle()));
            }
            if (item.getSubtitle().isEmpty()) {
                h.subtitle.setVisibility(View.GONE);
            } else {
                h.subtitle.setVisibility(View.VISIBLE);
                h.subtitle.setText(Html.fromHtml(item.getSubtitle()));
            }
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class SubMenuListHolder extends RecyclerView.ViewHolder {
        AppCompatTextView title, subtitle;

        public SubMenuListHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            subtitle = itemView.findViewById(R.id.subTitle);
        }
    }
}