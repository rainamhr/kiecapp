package np.edu.kiec.www.kiec.ui.testpreparation;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.util.ArrayList;

import np.edu.kiec.www.kiec.ui.home.menu.MainVisit;
import np.edu.kiec.www.kiec.ui.home.menu.MenuAdapter;
import np.edu.kiec.www.kiec.R;
import np.edu.kiec.www.kiec.utils.Constants;

/**
 * A simple {@link Fragment} subclass.
 */
public class TestPreparationFragment extends Fragment {


    public TestPreparationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Test Preparation");

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_test_preparation, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView list = (RecyclerView) view.findViewById(R.id.test_prep_recyclerview);

        Context context = getContext();

        list.setLayoutManager(
                new GridLayoutManager(context, 2)
        );

        list
                .addItemDecoration(new DividerItemDecoration(context,
                        LinearLayoutManager.HORIZONTAL));

        list
                .addItemDecoration(new DividerItemDecoration(context,
                        LinearLayoutManager.VERTICAL));        ArrayList<MainVisit> courses = new ArrayList<>();

        courses.add(new MainVisit(R.drawable.ic_home, "IELTS", Constants.IELTS));
        courses.add(new MainVisit(R.drawable.ic_toefl, "TOEFL", Constants.TOFEL));
        courses.add(new MainVisit(R.drawable.ic_pte, "PTE", Constants.PTE));
        courses.add(new MainVisit(R.drawable.ic_gre, "GRE", Constants.GRE));
        courses.add(new MainVisit(R.drawable.ic_gmat, "GMAT", Constants.GMAT));
        courses.add(new MainVisit(R.drawable.ic_sat, "SAT", Constants.SAT));


        list.setAdapter(new MenuAdapter(courses));
    }

}
