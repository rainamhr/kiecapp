package np.edu.kiec.www.kiec.ui.home.event;

/**
 * Created by sabin on 2/24/18.
 */

public class Event {
    private String date;
    private String title;
    private String locaiton;
    private String organizedBy;

    public Event(String date, String title, String locaiton, String organizedBy) {
        this.date = date;
        this.title = title;
        this.locaiton = locaiton;
        this.organizedBy = organizedBy;
    }

    public String getDate() {
        return date;
    }

    public String getTitle() {
        return title;
    }

    public String getLocaiton() {
        return locaiton;
    }

    public String getOrganizedBy() {
        return organizedBy;
    }
}
