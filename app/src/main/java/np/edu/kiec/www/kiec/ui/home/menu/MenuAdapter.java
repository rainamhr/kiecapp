package np.edu.kiec.www.kiec.ui.home.menu;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import np.edu.kiec.www.kiec.ui.base.AbstractViewHolder;
import np.edu.kiec.www.kiec.ui.SubMenuActivity;
import np.edu.kiec.www.kiec.databinding.ItemHomeMainBinding;

/**
 * Created by sabin on 2/25/18.
 */

public class MenuAdapter extends RecyclerView.Adapter<AbstractViewHolder> implements MenuInterface{
    List<MainVisit> mainVisitList;
    MenuPresenter menuPresenter;
    Context context;

    public MenuAdapter(List<MainVisit> mainVisitList) {
        this.mainVisitList = mainVisitList;
        menuPresenter = new MenuPresenter(this);
    }

    @Override
    public AbstractViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemHomeMainBinding itemHomeMainBinding =
                ItemHomeMainBinding.inflate(
                        LayoutInflater.from(parent.getContext()),
                        parent,
                        false
                );
        return new MainVisitItemViewHolder(itemHomeMainBinding);
    }

    @Override
    public void onBindViewHolder(AbstractViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return mainVisitList.size();
    }

    @Override
    public void onMenuClicked(int id) {
        Intent intent = new Intent(context, SubMenuActivity.class);
        intent.putExtra("id", id);

        context.startActivity(intent);
    }

    class MainVisitItemViewHolder extends AbstractViewHolder {
        ItemHomeMainBinding itemHomeMainBinding;

        public MainVisitItemViewHolder(ItemHomeMainBinding itemHomeMainBinding) {
            super(itemHomeMainBinding.getRoot());
            this.itemHomeMainBinding = itemHomeMainBinding;
            context = itemHomeMainBinding.getRoot().getContext();
        }

        @Override
        public void bind(int position) {
            MainVisit mainVisit = mainVisitList.get(position);
            Picasso.with(itemHomeMainBinding.getRoot().getContext())
                    .load(mainVisit.getImageResourceId())
                    .into(itemHomeMainBinding.homeMainImg);
            itemHomeMainBinding.setMain(mainVisit);
            itemHomeMainBinding.setPresenter(menuPresenter);
            itemHomeMainBinding.executePendingBindings();
        }
    }
}