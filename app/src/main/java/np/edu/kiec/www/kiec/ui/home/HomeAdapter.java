package np.edu.kiec.www.kiec.ui.home;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import np.edu.kiec.www.kiec.ui.base.AbstractViewHolder;
import np.edu.kiec.www.kiec.ui.home.banner.BannerSliderViewHolder;
import np.edu.kiec.www.kiec.ui.home.menu.MainVisitViewHolder;
import np.edu.kiec.www.kiec.databinding.ViewHolderBannerSliderBinding;
import np.edu.kiec.www.kiec.databinding.ViewHolderEventBinding;
import np.edu.kiec.www.kiec.databinding.ViewHolderMainBinding;
import np.edu.kiec.www.kiec.ui.home.event.EventViewHolder;

/**
 * Created by chirag on 11/30/17.
 */

public class HomeAdapter extends RecyclerView.Adapter<AbstractViewHolder> {
    private static final String TAG = "HomeAdaptor";

     private static final int KEY_BANNER_SLIDER = 0X1;
    private static final int KEY_MAIN_VISIT = 0X2;
    private static final int KEY_EVENTS = 0X3;



    @Override
    public int getItemViewType(int position) {

        switch (position) {
            case 0: return KEY_BANNER_SLIDER;
            case 1:
                return KEY_MAIN_VISIT;
            case 2:
                return KEY_EVENTS;
        }
        return position;
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    @Override
    public AbstractViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case KEY_BANNER_SLIDER:
                ViewHolderBannerSliderBinding viewHolderBannerSliderBinding =
                        ViewHolderBannerSliderBinding.inflate(layoutInflater, parent, false);
                return new BannerSliderViewHolder(viewHolderBannerSliderBinding);

            case KEY_MAIN_VISIT:
                ViewHolderMainBinding viewHolderMainBinding =
                        ViewHolderMainBinding.inflate(layoutInflater, parent, false);
                return new MainVisitViewHolder(viewHolderMainBinding);
            case KEY_EVENTS:
                ViewHolderEventBinding viewHolderEventsBinding =
                        ViewHolderEventBinding.inflate(layoutInflater, parent, false);
                return new EventViewHolder(viewHolderEventsBinding);
        }

        return null;
    }


    @Override
    public void onBindViewHolder(AbstractViewHolder holder, int position) {
        holder.bind(position);
    }




}
