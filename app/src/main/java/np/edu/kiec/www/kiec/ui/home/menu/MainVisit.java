package np.edu.kiec.www.kiec.ui.home.menu;

/**
 * Created by sabin on 2/24/18.
 */

public class MainVisit {
    private int imageResourceId;
    private String imageDescription;
    private int id;

    public MainVisit(int imageResourceId, String imageDescription, int id) {
        this.imageResourceId = imageResourceId;
        this.imageDescription = imageDescription;
        this.id = id;
    }

    public int getImageResourceId() {
        return imageResourceId;
    }

    public String getImageDescription() {
        return imageDescription;
    }

    public int getId() {
        return id;
    }
}
