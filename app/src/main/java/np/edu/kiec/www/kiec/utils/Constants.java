package np.edu.kiec.www.kiec.utils;

/**
 * Created by sabin on 2/25/18.
 */

public class Constants {
    public static final int CAREER_COUNSELING = 1;
    public static final int TEST_PREPARATION = 2;
    public static final int ABROAD_STUDIES = 3;
    public static final int BRANCHES = 4;

    public static final int TOFEL = 5;
    public static final int IELTS = 6;
    public static final int GRE = 7;
    public static final int GMAT = 8;
    public static final int SAT = 9;
    public static final int PTE = 10;
}
