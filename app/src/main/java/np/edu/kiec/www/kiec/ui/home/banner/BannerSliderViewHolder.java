package np.edu.kiec.www.kiec.ui.home.banner;

import java.util.ArrayList;
import java.util.List;

import np.edu.kiec.www.kiec.ui.base.AbstractViewHolder;
import np.edu.kiec.www.kiec.databinding.ViewHolderBannerSliderBinding;
import ss.com.bannerslider.banners.Banner;
import ss.com.bannerslider.banners.RemoteBanner;

/**
 * Created by sabin on 2/24/18.
 */

public class BannerSliderViewHolder extends AbstractViewHolder {
    ViewHolderBannerSliderBinding viewHolderBannerSliderBinding;

    public BannerSliderViewHolder(ViewHolderBannerSliderBinding viewHolderBannerSliderBinding) {
        super(viewHolderBannerSliderBinding.getRoot());
        this.viewHolderBannerSliderBinding = viewHolderBannerSliderBinding;
    }

    @Override
    public void bind(int position) {
        List<Banner> bannerList = new ArrayList<>();
        bannerList.add(new RemoteBanner("https://d2nsk6po0n2fai.cloudfront.net/wp-content/uploads/2016/08/study-abroad-banner.jpg"));
        bannerList.add(new RemoteBanner("https://d2nsk6po0n2fai.cloudfront.net/wp-content/uploads/2017/09/university-banner.jpg"));
        bannerList.add(new RemoteBanner("https://d2nsk6po0n2fai.cloudfront.net/wp-content/uploads/2018/02/study-in-3country.jpg"));
        bannerList.add(new RemoteBanner("https://d2nsk6po0n2fai.cloudfront.net/wp-content/uploads/2018/02/university.jpg"));

        viewHolderBannerSliderBinding.bannerSlider.setBanners(bannerList);
        viewHolderBannerSliderBinding.executePendingBindings();
    }
}
