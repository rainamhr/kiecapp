package np.edu.kiec.www.kiec.ui.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;


/**
 * Created by chirag on 12/4/17.
 */

public abstract class AbstractViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG = "AbstractViewHolder";

    public AbstractViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void bind(int position);
}
