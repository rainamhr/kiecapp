package np.edu.kiec.www.kiec.ui.abroad;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import np.edu.kiec.www.kiec.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AbroadStudyFragment extends Fragment {


    public AbroadStudyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getActivity().setTitle("Abroad Study");
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_abroad, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView abroadList = (RecyclerView) view;
        abroadList.setLayoutManager(new LinearLayoutManager(getActivity()));
        ArrayList<AbroadStudy> items = new ArrayList<>();
        items.add(new AbroadStudy("Study in USA").setSubtitle("The land of dreams and opportunities, the U.S. offers high-quality educational options…").setUrl("https://d2nsk6po0n2fai.cloudfront.net/wp-content/uploads/2016/09/USA-Flag.jpg"));
        items.add(new AbroadStudy("Study in Australia").setSubtitle("If you are interesting in studying in Australia then you are taking one step closer towards choosing…").setUrl("https://d2nsk6po0n2fai.cloudfront.net/wp-content/uploads/2016/09/australia.jpg"));
        items.add(new AbroadStudy("Study in New Zealand").setSubtitle("New Zealand study abroad programs will expose students to an amazing variety of breathtaking…").setUrl("https://d2nsk6po0n2fai.cloudfront.net/wp-content/uploads/2016/09/newzealand-flag.jpg"));
        items.add(new AbroadStudy("Study in Switzerland").setSubtitle("Switzerland study abroad programs offer students many options for personal and academic…").setUrl("https://d2nsk6po0n2fai.cloudfront.net/wp-content/uploads/2016/09/switzerland.jpg"));
        items.add(new AbroadStudy("Study in Ireland").setSubtitle("Despite being a relatively small nation, Ireland is jam-packed with things that make it a popular…").setUrl("https://d2nsk6po0n2fai.cloudfront.net/wp-content/uploads/2016/09/ireland-flag.jpg"));
        items.add(new AbroadStudy("Study in Japan").setSubtitle("The greatest appeal of studying in Japan is its academic environment where one can study…").setUrl("https://d2nsk6po0n2fai.cloudfront.net/wp-content/uploads/2016/09/Japan-flag.jpg"));
        items.add(new AbroadStudy("Study in Cyprus").setSubtitle("Welcome to Cyprus, an island of legends that basks year-round in the light…").setUrl("https://d2nsk6po0n2fai.cloudfront.net/wp-content/uploads/2016/09/cyprus.jpg"));
        items.add(new AbroadStudy("Study in Canda").setSubtitle("Canada rates extremely highly if you seek a high quality of life and education…").setUrl("https://d2nsk6po0n2fai.cloudfront.net/wp-content/uploads/2016/09/Canada.jpg"));
        items.add(new AbroadStudy("Study in Germany").setSubtitle("Germany officially the Federal Republic of Germany, is a country in Central Europe…").setUrl("https://d2nsk6po0n2fai.cloudfront.net/wp-content/uploads/2016/09/germany.jpg"));
        items.add(new AbroadStudy("Study in Finland").setSubtitle("Finland is situated in northern Europe and neighbors Sweden, Norway and Russia…").setUrl("https://d2nsk6po0n2fai.cloudfront.net/wp-content/uploads/2016/09/finland.jpg"));
        abroadList.setAdapter(new AbroadStudyAdapter(items));
    }


    class AbroadStudyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        List<AbroadStudy> abroadStudyList;

        AbroadStudyAdapter(List<AbroadStudy> abroadStudies) {
            this.abroadStudyList = abroadStudies;
        }

        @Override
        public int getItemViewType(int position) {
            super.getItemViewType(position);
            return position;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            return new DisplaySubViewHolder(layoutInflater.inflate(viewType % 2 == 0 ? R.layout.display_view_holder_item_left : R.layout.display_view_holder_item_right, parent, false));
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


            if (position % 2 == 0) {
                holder.itemView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
            } else {
                holder.itemView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
            }

            AbroadStudy item = abroadStudyList.get(position);
            ((DisplaySubViewHolder) holder).title.setText(item.getTitle());
            ((DisplaySubViewHolder) holder).subTitle.setText(item.getSubtitle());

            Picasso.with(((DisplaySubViewHolder) holder).icon.getContext())
                    .load(item.getUrl())
                    .into(((DisplaySubViewHolder) holder).icon);

        }

        @Override
        public int getItemCount() {
            return abroadStudyList.size();
        }
    }

    class DisplaySubViewHolder extends RecyclerView.ViewHolder {
        AppCompatImageView icon;
        AppCompatTextView title;
        AppCompatTextView subTitle;

        public DisplaySubViewHolder(View itemView) {
            super(itemView);
            icon = (AppCompatImageView) itemView.findViewById(R.id.icon);
            title = (AppCompatTextView) itemView.findViewById(R.id.title);
            subTitle = (AppCompatTextView) itemView.findViewById(R.id.subTitle);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
        }


    }


}
