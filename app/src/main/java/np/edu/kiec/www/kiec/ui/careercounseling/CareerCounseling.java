package np.edu.kiec.www.kiec.ui.careercounseling;

/**
 * Created by sabin on 2/25/18.
 */

public class CareerCounseling {
    private String title;
    private String subtitle;


    public CareerCounseling(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public CareerCounseling setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public CareerCounseling setSubtitle(String subtitle) {
        this.subtitle = subtitle;
        return this;
    }
}
