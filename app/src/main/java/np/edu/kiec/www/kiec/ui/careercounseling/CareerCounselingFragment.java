package np.edu.kiec.www.kiec.ui.careercounseling;


import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;


import java.util.ArrayList;

import np.edu.kiec.www.kiec.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CareerCounselingFragment extends Fragment {
    static ArrayList<CareerCounseling> firstDayItem = new ArrayList<>();
    static ArrayList<CareerCounseling> secondDayItem = new ArrayList<>();
    static ArrayList<CareerCounseling> thirdDayItem = new ArrayList<>();
    static ArrayList<CareerCounseling> fourthDayItem = new ArrayList<>();

    static {
        firstDayItem.add(new CareerCounseling("").setSubtitle("Your study abroad decision to the destination country is right choice if that is backed by your profile, background, preference and experience. This is your great and very important decision to pursuit your further career abroad. You may want abroad study either for further study, career and employment or personal enrichment with accredited by government or industry accreditation authority. For this you may need detailed information on the types of qualification and education framework of the destination country. Also when planning international study may keep in mind of your objective as well as availability of courses, their entry requirement as well as exist outcomes, costs, duration, content, work availability during study and so on.<br>" +
                "From abroad study you gain skills and qualifications for your professional career ahead by doing the course and other intangible outcomes like cultural experience of the destination country, contacts with local people and other international student’s cultural and sporting pursuits, travel and sightseeing and so on.<br>" +
                "Normally you will find two kinds of education marketing for overseas study<br>" +
                "Formal Education: This education that you will be gaining from abroad study.<br>" +
                "Study Travel and Tours: This is informal education for which you don’t need student visa."));


        firstDayItem.add(new CareerCounseling("Formal Education:").setSubtitle("Formal education consists of courses leading to a recognized qualification. Formal education providers normally must be accredited by the government or an industry accreditation authority.<br>" +
                "                Accredited education may include:<br>" +
                "        - Elementary, middle, secondary schools<br>" +
                "        - Vocational education and training<br>" +
                "        - Industry and professional training<br>" +
                "        - Polytechnics, institutes of technology<br>" +
                "        - Universities and advanced education<br>" +
                "        - Language schools.<br>" +
                "        Normally there are prescribed processes for acceptance and enrollment. Prospective students must meet academic and language prerequisites set by the educational institution, and be able to pay the relevant tuition fees. Enrollment must meet the legal requirements of the destination country."));


        secondDayItem.add(new CareerCounseling("Understanding a destination Country").setSubtitle("In order to take right decision student most understand destination countries properly under the following terms:<br>" +
                " - Location, Geography, Climate, and Environment<br>" +
                " - Population and History<br>" +
                " - Society and Culture<br>" +
                " - Government and Economy<br>" +
                " - Education System<br>" +
                " - Immigration and Visas<br>" +
                " - Living Conditions and Cost of Living<br>" +
                " - Pros and Cons"));

        secondDayItem.add(new CareerCounseling("Location, Geography, Climate, and Environment").setSubtitle("Students vary widely in their criteria for what makes a study destination attractive. The location, geography, climate, and environment of a country may be significant factors in a student choosing it over other options.<br>" +
                "For example, the country’s location relative to the student’s home country will largely determine the cost of travel for the student, the number of visits home, and so on. Its geography will influence the cost and ease of travel within the country, sightseeing opportunities, and other non-study activities. Its climate and environment are important in terms of comfort and quality of life for students."));
        secondDayItem.add(new CareerCounseling("Population and History").setSubtitle("Information about the population and history of a country is essential to understanding its character. When considering a destination country, the student should know about its demographics and the options for living in a large city, small city, or the countryside. Students may have preferences when it comes to how much they want to immerse themselves in local life, and so information regarding cultural diversity, cultural highlights/drawbacks, and how densely populated a destination is will help them focus their choices. The student should also possess some knowledge of the country’s history to gain a foundation for understanding the country’s character and social and cultural idiosyncrasies."));
        secondDayItem.add(new CareerCounseling("Society and Culture").setSubtitle("Each society has its own unique values and logic. The student need to know on how these may differ from those of their home country.<br>" +
                "If they are to live successfully in a foreign country while studying, students will need some understanding of the structure and institutions of its society, its socio-economic groupings, and its sub-cultures and communities. They will need to be aware of differences in etiquette and taboos, and what constitutes acceptable public language and behaviour.<br>" +
                "Each country offers a range of cultural experiences, which may constitute a primary motivation for a student to choose it as a study destination. A country’s culture and society may attract one student but be unattractive to another – every student is different.<br>" +
                "Some students may prefer a country where there are few cultural differences to distract them from their studies, but many will want to explore a country’s culture, arts, history, sports, or other dimensions. Again, this can be a prime motivating factor in many students’ choice of study destination, so the student should have a grasp of these elements of the study destination."));
        secondDayItem.add(new CareerCounseling("Government and Economy").setSubtitle("A stable government and healthy economy are highly desirable in a destination country. Most countries have national, regional, and/or local levels of government. Each has its own powers and jurisdictions. For example, the national government usually manages migration matters, including student visas, whereas local government’s building codes and fire and safety regulations directly affect the quality of education premises and student accommodation.<br>" +
                "The legal and judicial systems in the destination country may be quite different from those in the student’s home country. Laws may be stricter and penalties harsher. The student should aware that they will be subject to the destination country’s law while there.<br>" +
                "A healthy economy enhances financial security for both education institutions and students. It fosters job markets, providing employment opportunities for students whose visas permit them to work. Students will need to know how money is used in everyday transactions (e.g., cash, credit, direct debit, cheque), how to transfer it from their home bank, and exchange rates."));
        secondDayItem.add(new CareerCounseling("Education System").setSubtitle("The student \\ needs to know the structure of the destination country’s education system, its major institutions, the types of courses available to international students, and their cost. Students will need advice on the differences between the destination and home country’s education systems, and the equivalence and convertibility of qualifications between the two.<br>" +
                "The student should be aware of the reputation of the destination country for quality of educational outcomes, and be able to advise prospective students about the relative quality and cost of various course offerings. The student should find, read, and understand any agent manuals and website information prepared for them by educational institutions in the destination country in order to understand their courses and enrolment processes."));
        secondDayItem.add(new CareerCounseling("Immigration and Visas").setSubtitle("The destination country’s immigration and student visa regulations are essential knowledge for the student. He/she must know the types and requirements of visas available, how to prepare visa applications correctly, and the time frames and procedures for submitting visa applications.<br>" +
                "Prospective students will want to know about their visa options and the benefits and requirements of those options. For many students, being able to work will be a critical issue. Students should be told what documentation or other evidence is required for a student visa, and be given a clear time frame for preparing and submitting their application. For these you must have good student counselor in order to advice on all your requirement."));
        secondDayItem.add(new CareerCounseling("Living Conditions and Cost of Living").setSubtitle("The quality and cost of living are important considerations when choosing a destination country. The student need to know about the range of accommodation available and its cost. In particular, many students will be interested in home stay, others will want low-cost student accommodation (e.g., share house or apartment), and still others will want their own place. Students’ preferences will vary regarding the balance between the quality and cost of their living arrangements.<br>" +
                "In many countries, transport will be a significant cost for students. Unless they live close to their school, most students will have to travel daily by public transport . The student most take advise of concerning its accessibility and cost, and any alternative and affordable daily transport options. In addition to travel to and from their home country, many students will wish to take the opportunity to travel within the destination country (and surrounding countries) while they are abroad.<br>" +
                "The personal health and safety of students is paramount. The student should know about the risk of crime or any threats to their personal safety in the destination country. Unless the home and destination countries have a reciprocal health agreement, students will need health insurance (this is usually a visa condition).<br>" +
                "Students’ living expenses will vary with their personal budgets and spending habits. The student should know about the general costs of accommodation, transport, health care, food, clothing, and entertainment in the destination country."));


        thirdDayItem.add(new CareerCounseling("").setSubtitle("The student should know the equivalence of qualifications between the destination and home countries. It will allow student whether your existing qualifications are recognized in the destination country, and whether the destination country’s qualifications are recognized in the home country. He/she should know what the various sectors are called in the destination country, the age range, articulation points, entry requirements, etc. In order to analyze the equivalence of qualifications, student can check the characteristics of the qualification and courses for which it is awarded. These Characteristics might include the age of students, course entry requirements, length and content of courses, course outcomes, articulation to further study, and professional or industry recognition."));
        thirdDayItem.add(new CareerCounseling("<h2> Types of Qualification:</h2>School").setSubtitle("Two types of qualification are usually available from secondary schools: A leaving certificate or certificate of completion at the end of compulsory schooling (usually around 15 years of age or at the end of middle school). This is usually an informal certificate, but may be required for entry into vocational training programmes or as proof of education when seeking work.<br>" +
                "A high school certificate or diploma at the end of high school. This is required for matriculation to university. Some schools offer foundation programmes to international students instead of a high school certificate. This normally provides entry to specific universities."));
        thirdDayItem.add(new CareerCounseling("Vocational (Post-secondary)").setSubtitle("With such a broad range of training programmes, this sector usually offers a variety of certification ranging from certificates in basic job skills through to semi-professional diplomas. Qualifications awarded may include different forms of proficiency statements, certificates, and diplomas. The student should be careful to understand the equivalence of qualifications in different countries. In particular, the terms “certificate” and “diploma” can have very different usage in different qualifications frameworks."));
        thirdDayItem.add(new CareerCounseling("Proficiency Statement").setSubtitle("Proficiency statements indicate that a student has successfully completed part of a course and is competent in the units undertaken. This is not normally regarded as a qualification, but may be used for course credit later."));
        thirdDayItem.add(new CareerCounseling("Training Certificate").setSubtitle("Training certificates are awarded on successful completion of a recognized training course. There may be a number of levels of certificate (eg Certificate I, II, III, etc) corresponding to progressively higher levels of workplace skills."));
        thirdDayItem.add(new CareerCounseling("Trade Certificate").setSubtitle("Trade certificates are basically training certificates which confer a recognized trade licence. For example, completion of an apprenticeship makes the apprentice eligible for the trade certificate needed to work in the specified trade."));
        thirdDayItem.add(new CareerCounseling("Semi-professional Diploma").setSubtitle("Semi-professional diplomas are usually the highest level of vocational education. They generally qualify the holder for junior or lower middle management positions or equivalent. Semi-professional diplomas often articulate into professional degree courses at university."));
        thirdDayItem.add(new CareerCounseling("<h2>University</h2> Semi-professional Diploma").setSubtitle("Many universities offer semi-professional certificates and diplomas. In general these are the same as the semi-professional diplomas awarded in the vocational education sector."));
        thirdDayItem.add(new CareerCounseling("Bachelor’s Degree").setSubtitle("The basic qualification awarded by universities is the bachelor’s degree (e.g., Bachelor of Arts, Bachelor of Science) or equivalent. This is usually considered to be the basic professional qualification. An additional honors year may be a bridge to post-graduate study, and can be regarded as equivalent to the first year of a master’s degree."));
        thirdDayItem.add(new CareerCounseling("Post-graduate Diploma").setSubtitle("Post-graduate diplomas are a professional qualification, usually involving a degree of specialization, following on from a bachelor’s degree. For example, a student might follow a Bachelor of Arts degree with a Diploma of Education in order to qualify as a teacher."));
        thirdDayItem.add(new CareerCounseling("Master’s Degree").setSubtitle("Master’s degrees may be based on research, course work, or a combination of the two. A research methodology tends to be used more in master’s degrees which are oriented academically (e.g., Master of Science), whereas course work is more common in those with a professional orientation (e.g., Master of Business Administration)."));
        thirdDayItem.add(new CareerCounseling("Doctor of Philosophy (PhD)").setSubtitle("A doctoral degree is normally completed by research and dissertation. It is awarded as recognition that the candidate is capable of developing and undertaking independent research in the relevant discipline. A PhD is regarded as essential for an academic care er. Note: While most doctoral degrees are awarded as PhDs, there are variants such as Doctor of Divinity (DD). Doctor of Medicine (MD) is a professional degree and has its own methodology and processes."));
        thirdDayItem.add(new CareerCounseling("<h3>Language Schools</h3><h4>Proficiency Certificate</h4>").setSubtitle("Proficiency certificates are informal qualifications indicating the level of language proficiency students have attained. Students must sit one of the recognized language tests (e.g., IELTS, TOEFL) for a formal qualification in language proficiency."));
        thirdDayItem.add(new CareerCounseling("Other").setSubtitle("In most cases, international students cannot obtain a student visa for specialty colleges unless they are accredited for some form of the above qualifications."));

        fourthDayItem.add(new CareerCounseling("Planning International Study").setSubtitle("Students can have three main types of reasons students choose to study internationally, which will affect their preferences and objectives.<br>" +
                "Further study: The international study will lead to further study either internationally or at home. Career and employment: The international study will enhance the student’s career or employment prospects.<br>" +
                "Personal enrichment: The international study is the means by which the student can improve his/her quality of life, either through the course itself and/or through living in the destination country.<br>" +
                "When planning international study the student should take into account their preferences and objectives, as well as the availability of courses, their entry requirements, costs, duration, content, and outcomes. He/she should make sure courses are suitable for the prospective student in terms of both entry requirements and exit outcomes. In addition, he/she must take into account the logistics of travel, study, and living arrangements."));
        fourthDayItem.add(new CareerCounseling("Entry Requirements").setSubtitle("As a first step, the student should obtain information from the their existing qualifications, experience, and background. Then he/she should check course entry requirements and prerequisites. This information should be available in the course information provided by prospective educational institutions. The student must determine the equivalence of their qualifications to the course prerequisites, to ensure that the student’s academic and language proficiency matches the level he/she wishes to enter.<br>" +
                "If the student’s qualifications and experience meet the course entry requirements, the student should take advice from their counselor of the documentation needed to verify them for enrollment and visa applications.<br>" +
                "If the student do not meet entry requirements, the student must decide either to lower their study aims or to undertake some kind of bridging course to close the gap. This may be by means of a dedicated bridging or foundation course, or a lower-level course which articulates into the desired course. The student should develop a learning pathway consisting of such bridging courses and the desired course. Learning pathways are discussed below.<br>" +
                "International students must usually meet minimum language requirements, which are generally expressed in terms of internationally recognised language tests (e.g., IELTS, TOEFL). The student may therefore need to arrange testing of language proficiency, and if necessary, find a suitable language course. This may be a stand-alone language course, or a foundation course which includes a language component for this purpose."));
        fourthDayItem.add(new CareerCounseling("Outcomes").setSubtitle("In most cases, the outcomes are the reason the student wishes to take the course. The outcomes are the results of the course, i.e., the skills and qualifications that the student gains by doing the course, and/or any other intangible outcomes important to the student.<br>" +
                "For most students, the qualifications will be important either for employment or career purposes. For many students, the intangible outcomes will be as important as, if not more important than, the actual qualifications gained. Intangible outcomes include such things as experience in the culture of the destination country, contacts with local people and other international students, cultural or sporting pursuits, travel and sightseeing, and so on.<br>" +
                "The student must first establish the reasons for taking a course, what type of course(s) they are interested in, their cost base, and time frames. He/she can then recommend a course based on how well its outcomes fit the student’s aims and means."));
        fourthDayItem.add(new CareerCounseling("Logistics").setSubtitle("The student must ensure that they have the time and the means to undertake the proposed study. He/she should check the study plan has a realistic time-frame, including enrollment and visa processing time, travel times, and course duration. The cost of travel, study, and living must be affordable and accommodation must be suitable for the prospective student."));


    }

    private RecyclerView itemList;

    public CareerCounselingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Career Counseling");

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_career_counseling, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        final AppCompatTextView title = view.findViewById(R.id.title);

        itemList = view.findViewById(R.id.items);
        itemList.setLayoutManager(new LinearLayoutManager(getActivity()));
        final CareerCounselingAdapter adaptor = new CareerCounselingAdapter(firstDayItem);
        itemList.setAdapter(adaptor);


        AppCompatSpinner counselingDays = view.findViewById(R.id.counselingDays);
        final String[] days = getResources().getStringArray(R.array.counseling_days);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.counseling_days, R.id.title, days);
        counselingDays.setAdapter(adapter);
        counselingDays.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                title.setText(String.format("%s Day Counseling Campaign:", i == 0 ? "First" : i == 1 ? "Second" : i == 2 ? "Third" : "Fourth"));
                adaptor.setItems(i == 0 ? firstDayItem : i == 1 ? secondDayItem : i == 2 ? thirdDayItem : fourthDayItem);
                animateRecyclerView();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void animateRecyclerView() {
        if (itemList != null) {
            itemList.scrollToPosition(0);
            itemList.postDelayed(new Runnable() {
                @Override
                public void run() {

                    for (int i = 0; i < itemList.getChildCount(); i++) {
                        View view = itemList.getChildAt(i);
                        view.setPivotX(0.3F);
                        view.setPivotX(0.1F);
                        ObjectAnimator translate = ObjectAnimator.ofFloat(view, View.TRANSLATION_Y, 300, 0);
                        translate.setInterpolator(new DecelerateInterpolator(5));
                        translate.setDuration(400);
                        translate.start();
                    }

                }
            }, 100);
        }
    }
}
