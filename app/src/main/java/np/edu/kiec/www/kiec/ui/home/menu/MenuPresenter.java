package np.edu.kiec.www.kiec.ui.home.menu;

/**
 * Created by sabin on 2/25/18.
 */

public class MenuPresenter {
    MenuInterface menuInterface;

    MenuPresenter(MenuInterface menuInterface) {
        this.menuInterface = menuInterface;
    }

    public void onMenuClicked(int menuId) {
        menuInterface.onMenuClicked(menuId);
    }
}
